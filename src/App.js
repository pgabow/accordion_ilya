import './App.css';
import React, {useState, useEffect} from "react"
import AccordionItem from './components/AccordionItem';

const App = () => {
  const [data, setData] = useState([])
	//! для примера вынес тебе глобальную константу во внешний файл окружения
  // const USERS_URL = "https://jsonplaceholder.typicode.com/users"
  useEffect(() => {
      const request = async () => {
      const response = await fetch(process.env.REACT_APP_PUBLIC_API_USERS_URL)
      const data = await response.json()
      setData(data)
    }
    request()
  }, [])

	//? формирование данных для вывода лучше делать отдельно
	const accordionAll = data.map((user) => (
    <AccordionItem

      //! забыл задать уникальный key={user.id}
			//? его можно задать по разному. вариант что я сделал не самый хороший, но нам пока хватит

      key={user.id}
      id={user.id}
      name={user.name}
      username={user.username}
      phone={user.phone}
      website={user.website}
    />
  ))

  return (
    <div className='App'>
      <ul className='accordion'>
        {
          data.length > 0 && accordionAll
          // data.map((user) => (
          //   <AccordionItem
          //   id={user.id}
          //   name = {user.name}
          //   username={user.username}
          //   phone={user.phone}
          //   website={user.website}
          // />
          // ))
        }
      </ul>
    </div>
  )
}

export default App;
