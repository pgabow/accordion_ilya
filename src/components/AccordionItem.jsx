import React, { useState } from 'react'

const AccordionItem = ({ name, username, phone, website }) => {
  const [isActive, setIsActive] = useState(false)
  function handleActive() {
    setIsActive((prev) => !prev)
  }
	// каменты за собой убираем. оставляем только, если хочешь донести проверяющему или коллеге что-то уж очень важное
  // console.log(isActive)
  return (
    <li className='accordionItem'>
      <button className='button' onClick={handleActive}>
        {name}
        <span className='icon'>{isActive ? '-' : '+'}</span>
      </button>
      {isActive && (
        <div className={`content ${isActive ? 'open' : ''}`}>
          <h2>Username: {username}</h2>
          <h4>Phone - {phone}</h4>
          <p>Website - {website}</p>
        </div>
      )}
    </li>
  )
}

export default AccordionItem
